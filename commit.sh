set -e
# NOTE always use -o ci.skip to prevent refs from diverging
${UNFURL_CMD:-unfurl} -v --home '' export --format blueprint dummy-ensemble.yaml > unfurl-types.json
git add unfurl-types.json
if [ -n "$1" ]
then
git commit -m"$1"
git push -o ci.skip
fi
