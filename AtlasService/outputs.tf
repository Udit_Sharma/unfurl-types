output "username" {
    value = mongodbatlas_database_user.user.username
}

output "ip_address" {
    value = mongodbatlas_project_ip_access_list.ip.ip_address
}

output "connection_string" {
    value = mongodbatlas_cluster.cluster.connection_strings.0.standard_srv
    sensitive = true
}
