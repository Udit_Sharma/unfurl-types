variable instance_class {
    description = "The instance type of the RDS instance."
    default = "db.t2.micro"
}
variable engine {
    description = "The database engine to use."
    default = "mysql"
      }

variable engine_version  {
    description = "Engine Version"
    default = "5.1"
}

variable username {
    description = "Username for the master DB user."
    default = "onecommons"
}             
variable db_password {
    description = "Password for the master DB user."
    sensitive = true
}

variable "db_name" {
    description = "The name of the database to create when the DB instance is created. "
    default = "unfurl db"
  
}