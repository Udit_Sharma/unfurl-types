import re
from unfurl.configurator import Configurator

# arn:aws:ec2:us-west-2:284355325122:instance/i-0fe88f50a1a73b072
resource_arn_regex = r"^arn:aws:ec2:([A-z]|[0-9]|-)*:[0-9]*:instance/([A-z]|[0-9]|-)*$"
resource_arn_pattern = re.compile(resource_arn_regex)
aws_console_format = "https://console.aws.amazon.com/ec2/v2/home?region={region}#InstanceDetails:instanceId={instance_id}"


def get_console_url(ctx, args):
    """Get AWS console URL for a given ec2 resource ARN."""
    resource_arn = args["resource_arn"]

    if not resource_arn_pattern.match(resource_arn):
        raise ValueError(
            f"Invalid ec2 resource ARN, ec2 resource ARN must match {resource_arn_regex}")

    # id format: arn:aws:ec2:{{region}}:{{user}}:instance/{{id}}
    split = resource_arn.split(":")

    parsed = {
        "region": split[3],
        "instance_id": split[5].split("/")[1],
    }

    return aws_console_format.format(**parsed)


class MetadataConfigurator(Configurator):
    def run(self, task):
        task.logger.info("Fetching machine types")
        task.target.attributes["machine_types"] = list(
            self.all_machine_types(task))
        yield task.done(True)

    def can_dry_run(self, task):
        return True

    @staticmethod
    def all_machine_types(task):
        # XXX add parameter to filter by architecture
        # delay imports until now so the python package can be installed first
        import boto3
        from botocore.exceptions import BotoCoreError

        # for testing with moto, see if there's an endpoints_url set
        endpoint_url = task.query("$connections::AWSAccount::endpoints::ec2")
        client = boto3.client("ec2", endpoint_url=endpoint_url)
        try:
            filters = [
                dict(Name="processor-info.supported-architecture",
                     Values=["x86_64"]),
                dict(Name="supported-virtualization-type", Values=["hvm"]),
            ]
            paginator = client.get_paginator("describe_instance_types")
            for page in paginator.paginate(Filters=filters):
                yield from (
                    {
                        "name": it["InstanceType"],
                        "mem": it["MemoryInfo"]["SizeInMiB"],
                        "cpu": it["VCpuInfo"]["DefaultVCpus"],
                        "arch": it["ProcessorInfo"]["SupportedArchitectures"],
                    }
                    for it in page["InstanceTypes"]
                )
        except BotoCoreError as e:
            task.logger.error("AWS: %s", e)
            raise ValueError(
                "Can't find machine types. Can't communicate with AWS.")


if __name__ == "__main__":
    print(get_console_url(
        None, {"resource_arn": "arn:aws:ec2:us-west-2:284355325122:instance/i-0fe88f50a1a73b072"}))
